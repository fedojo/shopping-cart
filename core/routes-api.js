const express = require('express');
const router = express.Router();

const ProductRouter = require('./modules/product/router');
const CartRouter = require('./modules/cart/router');

router.use('/product', ProductRouter);
router.use('/cart', CartRouter);

module.exports = router;
