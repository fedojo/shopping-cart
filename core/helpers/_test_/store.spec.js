process.env.NODE_ENV = 'test';

const chai = require("chai");
const expect = require("chai").expect;
const reducer = require("../reducer");
const createStore = require("../createStore");
const {CREATE_PRODUCT} = require("../actions");

describe("store", () => {
    it("Should be created", () => {
        const store = createStore(reducer);

        expect(typeof store).to.equal("object")
    });

    it("Should add elements to store", () => {
        const store = createStore(reducer);

        store.dispatch({
            type: CREATE_PRODUCT,
            payload: {
                id: "asfd1234",
                name: "Name"
            }
        })

        expect(store.getState().products.length).to.equal(1);

        store.dispatch({
            type: CREATE_PRODUCT,
            payload: {
                id: "asfd12343",
                name: "Name 2"
            }
        });

        expect(store.getState().products.length).to.equal(2);
    });
});
