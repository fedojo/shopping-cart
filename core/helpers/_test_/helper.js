const product_1 = {
    id: 'id1',
    name: 'product_1'
}
const product_2 = {
    id: 'id2',
    name: 'product_2'
}
const product_3 = {
    id: 'id3',
    name: 'product_3'
}
const cart_1 = {
    id: 'cart_1_id',
    products: {}
}
const productToCartPayload_1 = {
    cartId: 'cart_1_id',
    productId: product_1.id,
    qty: 4
}
const productToCartPayload_2 = {
    cartId: 'cart_1_id',
    productId: product_2.id,
    qty: 2
}

const removeProductFromCartPayload_1 = {
    cartId: 'cart_1_id',
    productId: product_1.id,
}

const updateProductFromCartPayload_1 = {
    cartId: 'cart_1_id',
    productId: product_1.id,
    qty: 20
}

module.exports =  {
    product_1,
    product_2,
    product_3,

    cart_1,

    productToCartPayload_1,
    productToCartPayload_2,

    removeProductFromCartPayload_1,

    updateProductFromCartPayload_1
}
