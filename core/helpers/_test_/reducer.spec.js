process.env.NODE_ENV = 'test';

const expect = require("chai").expect;

const reducer = require("../reducer");
const {
    CREATE_PRODUCT,
    CREATE_CART,
    ADD_ITEM_TO_CART,
    REMOVE_ITEM_FROM_CART,
    UPDATE_ITEM_FROM_CART,
    CLEAR_PRODUCTS
} = require('../actions.js');

const {
    product_1,
    product_2,
    product_3,
    cart_1,
    productToCartPayload_1,
    productToCartPayload_2,
    removeProductFromCartPayload_1,
    updateProductFromCartPayload_1
} = require('./helper');

const initialState = {
    products: [],
    carts: {}
};

describe("reducer", () => {
    it("Should add one product to state", () => {
        const action = {
            type: CREATE_PRODUCT,
            payload: product_1
        }

        const reduced = reducer(initialState, action)

        expect(reduced.products.length).to.equal(1);
    });

    it("Should add one cart to state", () => {
        const action = {
            type: CREATE_CART,
            payload: cart_1
        }

        const reduced = reducer(initialState, action)

        expect(Object.keys(reduced.carts).length).to.equal(1);
    });

    it("Should add product cart to state", () => {
        const initialState = {
            products: [
                product_1,
                product_2,
                product_3
            ],
            carts: {
                [cart_1.id]: cart_1
            }
        };

        const action = {
            type: ADD_ITEM_TO_CART,
            payload: productToCartPayload_1
        }

        const reduced = reducer(initialState, action)

        expect(Object.keys(reduced.carts[cart_1.id].products).length).to.equal(1);
    });

    it("Should add 2 product cart to state", () => {
        const initialState = {
            products: [
                product_1,
                product_2,
                product_3
            ],
            carts: {
                [cart_1.id]: cart_1
            }
        };

        const action_1 = {
            type: ADD_ITEM_TO_CART,
            payload: productToCartPayload_1
        }

        const action_2 = {
            type: ADD_ITEM_TO_CART,
            payload: productToCartPayload_2
        }

        const reduced_1 = reducer(initialState, action_1)
        const reduced_2 = reducer(reduced_1, action_2)

        const lengthOfProductsInCart = Object.keys(reduced_2.carts[cart_1.id].products).length;

        expect(lengthOfProductsInCart).to.equal(2);
    });


    it("Should remove product from cart", () => {
        const initialState = {
            products: [
                product_1,
                product_2,
                product_3
            ],
            carts: {
                [cart_1.id]: {
                    id: cart_1.id,
                    products: {
                        [product_1.id]: 2,
                        [product_2.id]: 4
                    }
                }
            }
        };

        const action = {
            type: REMOVE_ITEM_FROM_CART,
            payload: removeProductFromCartPayload_1
        }

        const reduced = reducer(initialState, action);

        const lengthOfProductsInCart = Object.keys(reduced.carts[cart_1.id].products).length;

        expect(lengthOfProductsInCart).to.equal(1);
    });


    it("Should update cart item", () => {
        expect(true).to.equal(true)
        const initialState = {
            products: [
                product_1,
                product_2,
                product_3
            ],
            carts: {
                [cart_1.id]: {
                    id: cart_1.id,
                    products: {
                        [product_1.id]: 2,
                        [product_2.id]: 4
                    }
                }
            }
        };

        const action = {
            type: UPDATE_ITEM_FROM_CART,
            payload: updateProductFromCartPayload_1
        };

        const {
            cartId,
            productId,
            qty
        } = updateProductFromCartPayload_1;

        const reduced = reducer(initialState, action);

        const productsFromSelectedCart = reduced.carts[cartId].products;
        const lengthOfProductsInCart = Object.keys(productsFromSelectedCart).length;

        expect(lengthOfProductsInCart).to.equal(2);
        expect(productsFromSelectedCart[productId]).to.equal(qty);
    });

    it("Should clear products in store", () => {
        const initialState = {
            products: [
                product_1,
                product_2,
                product_3
            ],
            carts: {
                [cart_1.id]: {
                    id: cart_1.id,
                    products: {
                        [product_1.id]: 2,
                        [product_2.id]: 4
                    }
                }
            }
        };


        const action = {
            type: CLEAR_PRODUCTS
        }

        const reduced = reducer(initialState, action);

        const lengthOfProducts = Object.keys(reduced.products).length;

        expect(lengthOfProducts).to.equal(0);
    });
});
