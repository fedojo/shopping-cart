const createStore = require('./createStore');
const reducer = require('./reducer');

const storeInstance = createStore(reducer);

module.exports = storeInstance;
