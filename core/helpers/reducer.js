const {
    CREATE_PRODUCT,
    CREATE_CART,
    ADD_ITEM_TO_CART,
    REMOVE_ITEM_FROM_CART,
    UPDATE_ITEM_FROM_CART,
    CLEAR_PRODUCTS
} = require('./actions.js');

const initialState = {
    products: [],
    carts: {}
};

const reducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case CREATE_PRODUCT:
            state = {
                ...state,
                products: [
                    ...state.products,
                    payload
                ]
            }
            return state;

        case CREATE_CART:
            state = {
                ...state,
                carts: {
                    ...state.carts,
                    [payload.id]: payload
                }
            }
            return state;

        case ADD_ITEM_TO_CART:
            state = {
                ...state,
                carts: {
                    ...state.carts,
                    [payload.cartId]: {
                        id: payload.cartId,
                        products: {
                            ...state.carts[payload.cartId].products,
                            [payload.productId]: payload.qty
                        }
                    }
                }
            }
            return state;

        case REMOVE_ITEM_FROM_CART:
            const {
                cartId,
                productId
            } = payload;

            const removeProperties = (object, ...keys) => Object.entries(object).reduce((prev, [key, value]) => ({...prev, ...(!keys.includes(key) && { [key]: value }) }), {})

            const products = removeProperties(state.carts[cartId].products, productId)

            state = {
                ...state,
                carts: {
                    ...state.carts,
                    [cartId]: {
                        id: cartId,
                        products
                    }
                }
            }
            return state;

        case UPDATE_ITEM_FROM_CART:
            state = {
                ...state,
                carts: {
                    ...state.carts,
                    [payload.cartId] : {
                        ...state.carts[payload.cartId],
                        id: payload.cartId,
                        products: {
                            ...state.carts[payload.cartId].products,
                            [payload.productId]: payload.qty
                        }
                    }
                }
            }
            return state;

        case CLEAR_PRODUCTS:
            state = {
                ...state,
                products: []
            }
            return state;

        default:
            return state;
    }
};

module.exports = reducer;
