const express = require('express');
const http = require('http');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const ApiRouter = require('./routes-api');
const swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('../swagger.json');

const Server = {
    init: () => {
        Server.enableLogs();
        Server.enableRouting();
    },

    enableLogs: () => {
        app
            .use(bodyParser.json({limit: '10mb'}))
            .use(
                bodyParser.urlencoded({
                    extended: true,
                }),
            );
        if (process.env.NODE_ENV !== 'test') {
            app.use(morgan('combined'));
        }
    },


    enableRouting: () => {
        app
            .use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
            .use('/api', ApiRouter)
    },


    start: () => {
        Server.init();

        const server = http.createServer(app);

        server.listen(4000, () => {
            console.log('Server started');
        });

        return app;
    },

    stop: () => {
        server.close();
    },
};

module.exports = Server;
