process.env.NODE_ENV = 'test';

const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../../../../app");
const should = chai.should();
const expect = require("chai").expect;
const store = require('../../../helpers/store');
const mongoid = require('mongoid-js');
const {
    CLEAR_PRODUCTS,
    CREATE_PRODUCT
} = require('../../../helpers/actions');
chai.use(chaiHttp);

const endpointUrl = "/api/product/";

let id;

describe("Product REST", () => {
    describe("Product Endpoints", () => {
        it("it should create Product (method: POST)", done => {
            chai
                .request(app)
                .post(endpointUrl)
                .send({name: 'Product 1', price: 1123100, quantity: 10, description: 'Sample description'})
                .end((err, res) => {
                    const {body} = res;

                    expect(body).to.have.property("name");
                    expect(body).to.have.property("price");
                    expect(body).to.have.property("quantity");
                    expect(body).to.have.property("description");

                    id = res.body.id; // For next tests
                    done();
                });
        });

        it("it shouldn't create Product for unvalidated element (method: POST)", done => {
            chai
                .request(app)
                .post(endpointUrl)
                .send({name: 'Product 1', quantity: 10, description: 'Sample description'})
                .end((err, res) => {
                    const {body, status} = res;

                    expect(status).to.equal(500);
                    done();

                });
        });

        it("it should read Product (method: GET)", done => {
            chai
                .request(app)
                .get(endpointUrl + id)
                .end((err, res) => {
                    const {body} = res;

                    expect(body).to.have.property("name");
                    expect(body).to.have.property("price");
                    expect(body).to.have.property("quantity");
                    expect(body).to.have.property("description");

                    done();
                });
        });


        it("it shouldnt read Product when ID isnt in products list (method: GET)", done => {
            chai
                .request(app)
                .get(endpointUrl + '123asdf') // SAMPLE FAKE ID
                .end((err, res) => {
                    const {body} = res;

                    expect(body).to.have.property("error");

                    done();
                });
        });

        describe("List of Products (method: GET)", () => {
            before(done => {

                const models = [
                    {
                        id: mongoid(),
                        name: 'Product 1',
                        price: 1123100,
                        quantity: 11,
                        description: 'Sample description 1'
                    },
                    {
                        id: mongoid(),
                        name: 'Product 2',
                        price: 1123100,
                        quantity: 12,
                        description: 'Sample description 2'
                    },
                    {
                        id: mongoid(),
                        name: 'Product 3',
                        price: 1123100,
                        quantity: 13,
                        description: 'Sample description 3'
                    },
                ];

                store.dispatch({type: CLEAR_PRODUCTS});
                store.dispatch({type: CREATE_PRODUCT, payload: models[1]})
                store.dispatch({type: CREATE_PRODUCT, payload: models[2]})
                store.dispatch({type: CREATE_PRODUCT, payload: models[3]})

                done();
            });

            it("it should get list with all products", done => {
                chai
                    .request(app)
                    .get(endpointUrl)
                    .end((err, res) => {
                        const {body} = res;

                        // console.log('body.length',body.length)

                        expect(body.length).to.equal(3);

                        done();
                    });
            });
        });
    });
});
