const express = require('express');
const router = express.Router();
const {list, read, create, update, remove} = require('./ProductController');

router
    .get('/', list)
    .get('/:id', read)
    .post('/', create)
    // .put('/:id', update)
    // .delete('/:id', remove);

module.exports = router;
