const mongoid = require('mongoid-js');
const model = require('./ProductModel');
const storeInstance = require('../../helpers/store');
const _ = require('lodash');

const ProductController = {
    list(req, res) {
        res.send(storeInstance.getState().products);
    },

    create(req, res) {
        const newEl = {
            ...req.body,
            id: mongoid()
        }

        const {value, error} = model.validate(newEl);

        if (error) {
            res
                .status(500)
                .json(error);
        } else {
            storeInstance.dispatch({type: 'CREATE_PRODUCT', payload: newEl});
            res.json(value);
        }
    },

    read({params}, res) {
        const {products} = storeInstance.getState();
        const {id} = params;

        const el = _.find(products, {id});

        if (el) {
            res.json(el)
        } else {
            res.json({error: 'OBJECT NOT FOUND'})
        }
    }
}

module.exports = ProductController;
