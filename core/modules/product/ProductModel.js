const Joi = require('@hapi/joi');

const schema = Joi.object({
    id: Joi.string()
        .required(),

    name: Joi.string()
        .min(3)
        .max(30)
        .required(),

    price: Joi.number()
        .integer()
        .required(),

    quantity: Joi.number()
        .integer()
        .required(),

    description: Joi.string()
        .min(3)
        .max(3000)
});

module.exports = schema;
