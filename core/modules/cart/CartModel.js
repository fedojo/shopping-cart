const Joi = require('@hapi/joi');

const schema = Joi.object({
    id: Joi.string()
        .required(),

    items: Joi.array()
});

module.exports = schema;
