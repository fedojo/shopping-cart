const mongoid = require("mongoid-js");
const MODEL = require("./CartModel");
const _ = require('lodash')
const storeInstance = require('../../helpers/store');
const {
    CREATE_CART,
    ADD_ITEM_TO_CART,
    UPDATE_ITEM_FROM_CART,
    REMOVE_ITEM_FROM_CART
} = require('../../helpers/actions');

const CartController = {
    async list(req, res) {
        res.send(storeInstance.getState().carts);
    },

    async read({params}, res) {
        const {cartId} = params;
        const {carts} = storeInstance.getState();
        const singleCart = carts[cartId];

        if (typeof singleCart === 'undefined') {
            res.status(204).send('NO CART WITH THIS ID');
        } else {
            res.send(singleCart);
        }
    },

    async create(req, res) {
        const cart = {
            id: mongoid(),
            products: {}
        }

        const action = {
            type: CREATE_CART,
            payload: cart
        }

        storeInstance.dispatch(action);
        res.send(cart);
    },

    async addCartItem({params:{cartId}, body: {productId, qty}}, res) {
        const payload = {
            cartId,
            productId,
            qty
        }

        const action = {
            type: ADD_ITEM_TO_CART,
            payload
        }

        storeInstance.dispatch(action);

        res.send(storeInstance.getState().carts[cartId])
    },

    async listCartItems({cartId}, res) {
        const {carts} = storeInstance.getState();
        const singleCart = carts[cartId];
        const items = singleCart.items;
        res.send(items)
    },

    async updateCartItem(req, res) {
        res.send()
    },

    async removeCartItem(req, res) {
        res.send()
    }

};

module.exports = CartController;
