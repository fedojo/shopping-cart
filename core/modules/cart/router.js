const express = require('express');
const router = express.Router();
const {
    create,
    read,
    list,
    addCartItem,
    listCartItems,
    updateCartItem,
    removeCartItem
} = require('./CartController');

router
    .get('/', list)
    .get('/:cartId', read)
    .post('/', create)
    .post('/:cartId/items', addCartItem)
    .get('/:cartId/items', listCartItems)
    .put('/:cartId/items/:cartItemId', updateCartItem)
    .delete('/:cartId/items/:cartItemId', removeCartItem)

module.exports = router;
