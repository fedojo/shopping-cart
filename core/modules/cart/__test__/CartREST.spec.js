process.env.NODE_ENV = 'test';

//Require the dev-dependencies
const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../../../../app");
const should = chai.should();
const expect = require("chai").expect;
const store = require("../../../helpers/store");
const {
    product_1,
    product_2,
    product_3,

    cart_1,

    productToCartPayload_1,
    productToCartPayload_2,

    removeProductFromCartPayload_1,

    updateProductFromCartPayload_1
} = require("../../../helpers/_test_/helper")

chai.use(chaiHttp);

const userName = "tester@unit";
const endpointUrl = "/api/cart/";
let token;
let cartId;
let id;

//Our parent block
describe("Cart REST", () => {

    describe("Cart Endpoints", () => {
        it("it should create Cart (method: POST)", done => {
            chai
                .request(app)
                .post(endpointUrl)
                .send()
                .end((err, res) => {
                    const {body} = res;
                    cartId = body.id;

                    expect(typeof body.products).to.equal('object')
                    done();
                });
        });

        it("it should read Cart (method: GET)", done => {
            chai
                .request(app)
                .get(endpointUrl + cartId)
                .end((err, res) => {
                    const {body} = res;

                    expect(typeof body.products).to.equal('object')

                    done();
                });
        });

        it("it should add element to Cart (method: POST)", done => {
            const sending = {
                name: 'Product 1',
                price: 1123100,
                quantity: 10,
                description: 'Sample description'
            }

            chai
                .request(app)
                .post('/api/product/')
                .send(sending)
                .end((err, res) => {
                    const {body} = res;
                    const prodId = body.id;

                    chai
                        .request(app)
                        .post('/api/cart/')
                        .send()
                        .end((err, res) => {
                            const {body} = res;

                            const cartId = body.id; // For next tests

                            const countedEndpoint = endpointUrl + cartId + '/items';

                            chai
                                .request(app)
                                .post(countedEndpoint)
                                .send({productId: prodId, qty: 5})
                                .end((err, res) => {

                                    // TODO Check state

                                    expect(store.getState().carts[cartId].products[prodId]).to.equal(5);

                                    done();
                                });
                        });
                });


        });

        it("it should remove element from Cart (method: DELETE)", done => {

            const sending = {
                name: 'Product DING DONG',
                price: 1123100,
                quantity: 10,
                description: 'Sample description DING DONG'
            }

            chai
                .request(app)
                .post('/api/product/')
                .send(sending)
                .end((err, res) => {
                    const {body} = res;
                    const prodId = body.id;

                    chai
                        .request(app)
                        .post('/api/cart/')
                        .send()
                        .end((err, res) => {
                            const {body} = res;

                            const cartId = body.id; // For next tests

                            const countedEndpoint = endpointUrl + cartId + '/items';

                            chai
                                .request(app)
                                .post(countedEndpoint)
                                .send({productId: prodId, qty: 5})
                                .end((err, res) => {

                                    // TODO Check state


                                    chai
                                        .request(app)
                                        .delete('/api/cart/' + cartId)
                                        .send()
                                        .end((err, res) => {
                                            const {body} = res;

                                            const cartId = body.id; // For next tests

                                            expect(typeof store.getState().carts[cartId]).to.be.equal('undefined')
                                            done();
                                        });

                                });
                        });
                });


        });


    });
});
